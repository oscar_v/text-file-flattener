// This program performs jallas bidding

package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/ardanlabs/conf"
	"github.com/gocolly/colly"

	"github.com/pkg/errors"
)

func main() {
	if err := run(); err != nil {
		log.Printf("error: %s", err)
		os.Exit(1)
	}
}

func run() error {
	// =========================================================================
	// Configuration

	var cfg struct {
		Args conf.Args
	}

	if err := conf.Parse(os.Args[1:], "ADMIN", &cfg); err != nil {
		if err == conf.ErrHelpWanted {
			usage, err := conf.Usage("ADMIN", &cfg)
			if err != nil {
				return errors.Wrap(err, "generating usage")
			}
			fmt.Println(usage)
			return nil
		}
		return errors.Wrap(err, "error: parsing config")
	}

	var err error
	switch cfg.Args.Num(0) {
	case "flatten":
		err = flatten(cfg.Args.Num(1))
	case "search":
		err = search(cfg.Args.Num(1), cfg.Args.Num(2))
	default:
		err = errors.New("Must specify a command")
	}

	if err != nil {
		return err
	}

	return nil
}

func flatten(path string) error {
	if path == "" {
		path = "."
	}

	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}

	var textFileNames []string

	for _, file := range files {
		if strings.Contains(file.Name(), ".txt") {
			textFileNames = append(textFileNames, file.Name())
		}
	}

	dupeCheck := make(map[string]bool)
	finalAllowList := []string{}

	for _, fileName := range textFileNames {
		// Open file and create scanner on top of it
		file, err := os.Open(fileName)
		if err != nil {
			return err
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			var sanitizedLine string
			splitLine := strings.Split(scanner.Text(), ",")
			sliceLength := len(splitLine)

			sanitizedLine = splitLine[sliceLength-1]

			if sliceLength > 1 {
				sanitizedLine = strings.TrimSpace(sanitizedLine)
			}

			if len(sanitizedLine) > 0 {
				if _, ok := dupeCheck[sanitizedLine]; !ok {
					finalAllowList = append(finalAllowList, sanitizedLine)
					sort.Strings(finalAllowList)
				}

				dupeCheck[sanitizedLine] = true
			}
		}
	}

	file, err := os.OpenFile("outputFlatten.txt", os.O_CREATE|os.O_WRONLY, 0644)

	if err != nil {
		return err
	}

	datawriter := bufio.NewWriter(file)

	for _, data := range finalAllowList {
		//since this app is for windows usage, we need to add \r
		_, _ = datawriter.WriteString(data + "\r\n")
	}

	datawriter.Flush()
	file.Close()

	return nil
}

func search(searchTerm, inPath string) error {
	if inPath == "" {
		inPath = "."
	}

	files, err := ioutil.ReadDir(inPath)
	if err != nil {
		return err
	}

	dir, err := filepath.Abs(filepath.Dir(inPath))
	if err != nil {
		panic(err)
	}

	t := &http.Transport{}
	t.RegisterProtocol("file", http.NewFileTransport(http.Dir("/")))

	c := colly.NewCollector()
	c.WithTransport(t)

	var fileNamesWithMatchingTerm []string
	var currentFileName string

	c.OnHTML("#cover", func(e *colly.HTMLElement) {
		if strings.Contains(e.ChildText("p"), searchTerm) {
			fileNamesWithMatchingTerm = append(fileNamesWithMatchingTerm, currentFileName)
		}
	})

	for _, file := range files {
		currentFileName = file.Name()

		if strings.HasSuffix(file.Name(), ".html") {
			path := "file://" + dir + "/" + file.Name()
			c.Visit(path)
		}
	}

	file, err := os.OpenFile("outputSearch.txt", os.O_CREATE|os.O_WRONLY, 0644)

	if err != nil {
		return err
	}

	datawriter := bufio.NewWriter(file)

	for _, data := range fileNamesWithMatchingTerm {
		//since this app is for windows usage, we need to add \r
		_, _ = datawriter.WriteString(data + "\r\n")
	}

	lengthOutput := fmt.Sprintf("%d files matched the search term: %s", len(fileNamesWithMatchingTerm), searchTerm)

	_, _ = datawriter.WriteString("\r\n" + lengthOutput)
	datawriter.Flush()
	file.Close()

	return nil
}
